import * as got from "got"
import { pipeline } from "stream"
import { promisify } from "util"
import { createWriteStream } from "fs"
import extract from "extract-zip"

const pipelineAsync = promisify(pipeline)
const extractAsync = promisify(extract)

export async function download() {
  const fn = __dirname + "/dc.zip"
  await pipelineAsync(
    got.stream("http://www.aboutmyx.com/files/DeltaCopyRaw.zip"),
    createWriteStream(fn)
  )
  await extractAsync(fn, { dir: `${__dirname}/dc` })
}

if (require.main === module) {
  download().catch(err => {
    console.error(err)
    process.exit(1)
  })
}
