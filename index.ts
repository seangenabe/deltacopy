import { join } from "path"

export const path = join(__dirname, "dc")
export const rsync = join(path, "rsync.exe")
